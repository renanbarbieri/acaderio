package hbsoft.com.br.acaderio.fragment;

import android.app.Activity;
import android.app.DialogFragment;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import hbsoft.com.br.acaderio.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnCommentListener} interface
 * to handle interaction events.
 * Use the {@link AddCommentFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddCommentFragment extends DialogFragment {
    public static final String FRAGMENT_TAG = "hbsoft.com.br.acaderio.fragment.AddCommentFragment";
    private static final String ARG_PARAM_COMMENT = "hbsoft.com.br.acaderio.fragment.AddCommentFragment.ARG_PARAM_COMMENT";

    TextView textComment;
    private String comment;

    private OnCommentListener callback;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param comment Parameter 1.
     * @return A new instance of fragment AddCommentFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AddCommentFragment newInstance(String comment) {
        AddCommentFragment fragment = new AddCommentFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM_COMMENT, comment);
        fragment.setArguments(args);
        return fragment;
    }

    public AddCommentFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            comment = getArguments().getString(ARG_PARAM_COMMENT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setStyle(DialogFragment.STYLE_NORMAL, DialogFragment.STYLE_NO_TITLE);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        // Inflate the layout for this fragment
        View contentView = inflater.inflate(R.layout.fragment_add_comment, container, false);

        textComment = (TextView) contentView.findViewById(R.id.editComment);
        Button btSave = (Button) contentView.findViewById(R.id.btSave);

        if(comment!=null){
            textComment.setText(comment);
        }

        btSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(textComment.getText()!=null) {
                    callback.onCommentDone(textComment.getText().toString());
                    AddCommentFragment.this.dismiss();
                }
            }
        });

        return contentView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            callback = (OnCommentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnCommentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callback = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     */
    public interface OnCommentListener {
        public void onCommentDone(String comment);
    }

}
