package hbsoft.com.br.acaderio;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;

import com.parse.ParseUser;

import java.text.DecimalFormat;

/**
 * Created by Renan on 08/10/2015.
 */
public class BaseActivity extends AppCompatActivity {

    private static final float MAX_OPACITY = 0.80f;

    private ProgressDialog dialogLoading;

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        setStatusBarColor(getColor(R.color.graySoft));
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void setStatusBarColor(int color){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            // finally change the color
            window.setStatusBarColor(color);
        }
    }

    protected void startLoadingDialog(){
        dialogLoading = new ProgressDialog(this);
        dialogLoading.setMessage(getString(R.string.textLoading));
        dialogLoading.setIndeterminate(true);
        dialogLoading.setCancelable(false);
        dialogLoading.show();
    }

    protected void stopLoadingDialog(){
        if(dialogLoading.isShowing()){
            dialogLoading.dismiss();
        }
    }

    protected void changeAlphaActionBar(float alpha, int colorId){
        ColorDrawable colorDrawable = new ColorDrawable(getColor(colorId));
        colorDrawable.setAlpha((int) (alpha * MAX_OPACITY));

        if(getSupportActionBar()!=null) {
            getSupportActionBar().setBackgroundDrawable(colorDrawable);
        }
        else{
            Log.e("changeAlphaActionBar", "actionBar is null");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_list_gym, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id){
            case R.id.actionLogout:
                ParseUser.logOut();
                Intent intentGymAct = new Intent(this, LoginActivity.class);
                intentGymAct.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intentGymAct);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
