package hbsoft.com.br.acaderio;

import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseQuery;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import hbsoft.com.br.acaderio.model.Gym;

public class ListGymActivity extends BaseActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{

    private GymAdapter mAdapter;

    private GoogleApiClient googleApiClient;
    private Location lastLocation;
    private ParseGeoPoint userLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_gym);
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.recyclerGyms);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(false);
        // use a linear layout manager
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        // specify an adapter (see also next example)
        mAdapter = new GymAdapter();
        mRecyclerView.setAdapter(mAdapter);
        buildGoogleApiClient();
//        getGyms();
    }

    public void getGyms(){
        startLoadingDialog();
        ParseQuery<Gym> queryGetGyms = ParseQuery.getQuery(Gym.class);
        queryGetGyms.findInBackground(new FindCallback<Gym>() {
            @Override
            public void done(List<Gym> gyms, ParseException error) {
                if (error == null) {
                    Log.i("ListGym", "gyms: " + gyms.size());
                    mAdapter.populateList(gyms);
                } else {
                    Log.e("ListGym", "error: " + error.getMessage());
                }
                stopLoadingDialog();
            }
        });
    }

    public void getNearGyms(Location currentLocation){
        userLocation = new ParseGeoPoint(currentLocation.getLatitude(), currentLocation.getLongitude());
        startLoadingDialog();
        ParseQuery<Gym> queryGetGyms = ParseQuery.getQuery(Gym.class);
        queryGetGyms.whereNear("geoLocation", userLocation);
        queryGetGyms.setLimit(50);
        queryGetGyms.findInBackground(new FindCallback<Gym>() {
            @Override
            public void done(List<Gym> gyms, ParseException error) {
                if (error == null) {
                    Log.i("ListGym", "gyms: " + gyms.size());
                    mAdapter.populateList(gyms);
                } else {
                    Log.e("ListGym", "error: " + error.getMessage());
                }
                stopLoadingDialog();
            }
        });
    }

    private synchronized void buildGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
        Log.i("buildGoogleApiClient", "buildGoogleApiClient");
    }

    @Override
    public void onConnected(Bundle bundle) {
        lastLocation = LocationServices.FusedLocationApi.getLastLocation(
                googleApiClient);
        if(lastLocation != null) {
            Log.i("onConnected", "Getting near gyms");
            getNearGyms(lastLocation);
        }
        else{
            Log.i("onConnected", "Getting gyms");
            getGyms();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i("onConnectionSuspended", "Connection suspended: "+i);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i("onConnectionFailed", "ConnectionFailed: "+connectionResult.getErrorCode());
    }

    public class GymAdapter extends RecyclerView.Adapter<GymAdapter.ViewHolder> {
        private List<Gym> gyms;

        // Provide a reference to the views for each data item
        // Complex data items may need more than one view per item, and
        // you provide access to all the views for a data item in a view holder
        public class ViewHolder extends RecyclerView.ViewHolder {
            // each data item is just a string in this case
            public TextView textLocal, textDistance;
            public ImageButton btGetDirections;
            public View itemLayout;

            public ViewHolder(View v) {
                super(v);

                this.itemLayout = v;
                this.btGetDirections = (ImageButton) v.findViewById(R.id.btGetDirections);
                this.textLocal = (TextView) v.findViewById(R.id.textLocal);
                this.textDistance = (TextView) v.findViewById(R.id.textDistance);
            }

        }

        // Provide a suitable constructor (depends on the kind of dataset)
        public GymAdapter() {
            gyms = new ArrayList<Gym>();
        }

        public void populateList(List<Gym> gyms){
            this.gyms.clear();
            this.gyms.addAll(gyms);
            this.notifyDataSetChanged();
        }

        // Create new views (invoked by the layout manager)
        @Override
        public GymAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, final int viewType) {
            // create a new view
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_adapter_gym, parent, false);
            // set the view's size, margins, paddings and layout parameters

            return new ViewHolder(v);
        }

        // Replace the contents of a view (invoked by the layout manager)
        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {
            // - get element from your dataset at this position
            // - replace the contents of the view with that element
            holder.textLocal.setText(gyms.get(position).getLocal());

            holder.textDistance.setText(getKilometersDistanceString(gyms.get(position)));

            holder.itemLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intentGymAct = new Intent(ListGymActivity.this, GymActivity.class);
                    Bundle params = new Bundle();
                    params.putString(GymActivity.ARGS_GYM, gyms.get(position).getObjectId());
                    intentGymAct.putExtras(params);

                    startActivity(intentGymAct);
                }
            });

            holder.btGetDirections.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                    Uri.parse("http://maps.google.com/maps?daddr=" + gyms.get(position).getGeoLocation().getLatitude() + "," + gyms.get(position).getGeoLocation().getLongitude()));
                    startActivity(intent);
                }
            });

        }

        private String getKilometersDistanceString(Gym gym){
            double kilometers = gym.getGeoLocation().distanceInKilometersTo(userLocation);
            DecimalFormat fmt = new DecimalFormat("0.00");
            String stringKm = fmt.format(kilometers);
            return  stringKm+" Km";
        }

        // Return the size of your dataset (invoked by the layout manager)
        @Override
        public int getItemCount() {
            return gyms.size();
        }

    }
}
