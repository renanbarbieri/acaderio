package hbsoft.com.br.acaderio.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.parse.FindCallback;
import com.parse.ParseClassName;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.io.Serializable;

/**
 * Created by Renan on 08/10/2015.
 */
@ParseClassName("Gym")
public class Gym extends ParseObject{

    public String getLocal(){
        return getString("local");
    }

    public void setLocal(String local){
        put("local", local);
    }

    public String getNeighborhood(){
        return getString("neighborhood");
    }

    public void setNeighborhood(String neighborhood){
        put("neighborhood", neighborhood);
    }

    public String getReference(){
        return getString("reference");
    }

    public void setReference(String reference){
        put("reference", reference);
    }

    public int getStreetNumber(){
        return getInt("streetNumber");
    }

    public void setStreetNumber(int streetNumber){
        put("streetNumber", streetNumber);
    }

    public ParseGeoPoint getGeoLocation(){
        return getParseGeoPoint("geoLocation");
    }

    public void setGeoLocation(ParseGeoPoint geoLocation){
        put("geoLocation", geoLocation);
    }

    public void addLike(){
        put("likes", getLikes()+1);
    }

    public void addDislike(){
        put("dislikes", getDislikes()+1);
    }

    public int getDislikes(){
        return getInt("dislikes");
    }

    public int getLikes(){
        return getInt("likes");
    }

    public void getRelationWithUser(FindCallback<GymUser> callback){
        ParseQuery<GymUser> query = ParseQuery.getQuery(GymUser.class);
        query.whereEqualTo("gym", this);
        query.findInBackground(callback);
    }
}
