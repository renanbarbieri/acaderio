package hbsoft.com.br.acaderio.model;

import com.parse.GetCallback;
import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

/**
 * Created by Renan on 18/10/2015.
 */
@ParseClassName("GymUser")
public class GymUser extends ParseObject {

    public void setComment(String comment){
        put("comment", comment);
    }

    public String getComment(){
        return getString("comment");
    }

    public void setUser(ParseUser user){
        put("user", user);
    }

    public ParseUser getUser(){
        return getParseUser("user");
    }

    public void setGym(Gym gym){
        put("gym", gym);
    }

    public Gym getGym(){
        return (Gym) get("gym");
    }

    public void setAlreadyLiked(boolean liked){
        put("alreadyLiked", liked);
    }

    public boolean isAlreadyLiked(){
        return getBoolean("alreadyLiked");
    }

    public static void getRelationsByUser(ParseUser user, Gym gym, GetCallback<GymUser> callback){
        ParseQuery<GymUser> query = ParseQuery.getQuery(GymUser.class);
        query.whereEqualTo("user", user);
        query.whereEqualTo("gym", gym);
        query.getFirstInBackground(callback);
    }

}
