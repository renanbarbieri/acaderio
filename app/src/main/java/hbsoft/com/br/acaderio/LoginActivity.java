package hbsoft.com.br.acaderio;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

public class LoginActivity extends BaseActivity implements View.OnClickListener{

    EditText editUsername, editPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        editUsername = (EditText) findViewById(R.id.editUsername);
        editPassword = (EditText) findViewById(R.id.editPassword);

        Button btLogin = (Button) findViewById(R.id.btLogin);
        Button btSignin = (Button) findViewById(R.id.btSignin);

        btLogin.setOnClickListener(this);
        btSignin.setOnClickListener(this);

        if(ParseUser.getCurrentUser()!=null){
            openListGymActivity();
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btLogin:
                doLogin(editUsername.getText().toString(), editPassword.getText().toString());
                break;
            case R.id.btSignin:
                Intent intentGymAct = new Intent(this, SigninActivity.class);
                startActivity(intentGymAct);
                break;
        }
    }

    private void doLogin(final String username, final String password){
        startLoadingDialog();
        ParseUser.logInInBackground(username, password, new LogInCallback() {
            public void done(ParseUser user, ParseException e) {
                stopLoadingDialog();
                if (user != null) {
                    // Hooray! The user is logged in.
                    openListGymActivity();

                } else {
                    // Signup failed. Look at the ParseException to see what happened.
                    Toast.makeText(LoginActivity.this, "Ocorreu um erro ao fazer o login: "+e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void openListGymActivity(){
        Intent intentGymAct = new Intent(LoginActivity.this, ListGymActivity.class);
        intentGymAct.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intentGymAct);
    }
}
