package hbsoft.com.br.acaderio;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

public class SigninActivity extends AppCompatActivity {

    EditText editEmail, editPassword, editUsername;
    private ProgressDialog dialogLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);

        editUsername = (EditText) findViewById(R.id.editUsername);
        editEmail = (EditText) findViewById(R.id.editEmail);
        editPassword = (EditText) findViewById(R.id.editPassword);

        Button btFinish = (Button) findViewById(R.id.btSignin);

        btFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doSignin(
                        editUsername.getText().toString(),
                        editEmail.getText().toString(),
                        editPassword.getText().toString()
                );
            }
        });

    }

    private void doSignin(String username, String email, String password){
        startLoadingDialog();
        ParseUser user = new ParseUser();
        user.setUsername(username);
        user.setPassword(password);
        user.setEmail(email);

        user.signUpInBackground(new SignUpCallback() {
            public void done(ParseException e) {
                stopLoadingDialog();
                if (e == null) {
                    // Hooray! The user is logged in.
                    Intent intentGymAct = new Intent(SigninActivity.this, ListGymActivity.class);
                    intentGymAct.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intentGymAct);
                } else {
                    // Signup failed. Look at the ParseException to see what happened.
                    Toast.makeText(SigninActivity.this, "Ocorreu um erro ao fazer o cadastro: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    protected void startLoadingDialog(){
        dialogLoading = new ProgressDialog(this);
        dialogLoading.setMessage(getString(R.string.textLoading));
        dialogLoading.setIndeterminate(true);
        dialogLoading.setCancelable(false);
        dialogLoading.show();
    }

    protected void stopLoadingDialog(){
        if(dialogLoading.isShowing()){
            dialogLoading.dismiss();
        }
    }
}
