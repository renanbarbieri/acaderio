package hbsoft.com.br.acaderio;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import hbsoft.com.br.acaderio.fragment.AddCommentFragment;
import hbsoft.com.br.acaderio.model.Gym;
import hbsoft.com.br.acaderio.model.GymUser;

public class GymActivity extends BaseActivity implements View.OnClickListener,
            AddCommentFragment.OnCommentListener, OnMapReadyCallback{

    public static final String ARGS_GYM = "hbsoft.com.br.acaderio.GymActivity.ARGS_GYM";

    private Gym gym;
    private List<GymUser> gymRelations;

    private ListView listComments;
    private TextView textGymLocal, textGymReference, textGymNeighborhood, textCountDislikes, textCountLikes;
    private ImageButton imgBtAddComment;
    private CommentAdapter commentAdapter;
    private LinearLayout layoutLike, layoutDislike;

    private MapFragment mapFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gym);
        findViews();

        Bundle receivedArgs = getIntent().getExtras();

        if(receivedArgs != null){
            startLoadingDialog();
            ParseQuery<Gym> query = ParseQuery.getQuery(Gym.class);
            query.getInBackground(receivedArgs.getString(ARGS_GYM), new GetCallback<Gym>() {
                public void done(Gym gym, ParseException e) {
                    if (e == null) {
                        GymActivity.this.gym = gym;
                        gym.getRelationWithUser(new FindCallback<GymUser>() {
                            @Override
                            public void done(List<GymUser> relations, ParseException e) {
                                stopLoadingDialog();
                                if (e == null) {
                                    GymActivity.this.gymRelations = relations;
                                    populateScreen();
                                } else {
                                    // something went wrong
                                    Toast.makeText(GymActivity.this, getString(R.string.errorLoading), Toast.LENGTH_LONG).show();
                                    GymActivity.this.finish();
                                }
                            }
                        });
                    } else {
                        stopLoadingDialog();
                        // something went wrong
                        Toast.makeText(GymActivity.this, getString(R.string.errorLoading), Toast.LENGTH_LONG).show();
                        GymActivity.this.finish();
                    }
                }
            });
        }

    }

    private void findViews(){
        listComments = (ListView) findViewById(R.id.listViewComments);

        textGymLocal = (TextView) findViewById(R.id.textLocal);
        textGymNeighborhood = (TextView) findViewById(R.id.textNeighborhood);
        textGymReference = (TextView) findViewById(R.id.textReference);
        textCountDislikes = (TextView) findViewById(R.id.textCountDislikes);
        textCountLikes = (TextView) findViewById(R.id.textCountLikes);

        layoutDislike = (LinearLayout) findViewById(R.id.layoutDislike);
        layoutLike = (LinearLayout) findViewById(R.id.layoutLike);

        imgBtAddComment = (ImageButton) findViewById(R.id.btAddComment);

        mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.fragmentMap);
    }

    private void populateScreen(){
        textGymLocal.setText(gym.getLocal());
        textGymNeighborhood.setText(getString(R.string.textLabelNeighborhood)+" "+gym.getNeighborhood());
        textGymReference.setText(getString(R.string.textLabelReference)+" "+gym.getReference());
        textCountDislikes.setText(String.valueOf(gym.getDislikes()));
        textCountLikes.setText(String.valueOf(gym.getLikes()));

        commentAdapter = new CommentAdapter(this.gymRelations);
        listComments.setAdapter(commentAdapter);
        setListViewHeightBasedOnChildren(listComments);

        imgBtAddComment.setOnClickListener(this);
        layoutDislike.setOnClickListener(this);
        layoutLike.setOnClickListener(this);

        mapFragment.getMapAsync(this);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.layoutDislike:
                gym.addDislike();
                gym.saveInBackground();
                textCountDislikes.setText(String.valueOf(gym.getDislikes()));
                break;
            case R.id.layoutLike:
                gym.addLike();
                gym.saveInBackground();
                textCountLikes.setText(String.valueOf(gym.getLikes()));
                break;
            case R.id.btAddComment:
                startLoadingDialog();
                GymUser.getRelationsByUser(ParseUser.getCurrentUser(), gym, new GetCallback<GymUser>() {
                    @Override
                    public void done(GymUser gymUser, ParseException e) {
                        stopLoadingDialog();
                        AddCommentFragment addCommentFragment;
                        if (e == null) {
                            addCommentFragment = AddCommentFragment.newInstance(gymUser.getComment());
                            addCommentFragment.show(getFragmentManager(), AddCommentFragment.FRAGMENT_TAG);
                        } else {
                            if (e.getCode() == ParseException.OBJECT_NOT_FOUND) {
                                addCommentFragment = AddCommentFragment.newInstance(null);
                                addCommentFragment.show(getFragmentManager(), AddCommentFragment.FRAGMENT_TAG);
                            } else {
                                Log.e("getRelationsByUser", e.getMessage());
                                Toast.makeText(GymActivity.this, "Erro: " + e.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                });
                break;
        }
    }

    @Override
    public void onCommentDone(final String comment) {
        startLoadingDialog();
        GymUser.getRelationsByUser(ParseUser.getCurrentUser(), gym, new GetCallback<GymUser>() {
            @Override
            public void done(GymUser gymUser, ParseException e) {
                stopLoadingDialog();
                GymUser currGymUser;
                if(e == null){
                    currGymUser = gymUser;
                    currGymUser.setComment(comment);
                    currGymUser.saveInBackground();
                }
                else{
                    if (e.getCode() == ParseException.OBJECT_NOT_FOUND) {
                        currGymUser = new GymUser();
                        currGymUser.setUser(ParseUser.getCurrentUser());
                        currGymUser.setGym(GymActivity.this.gym);
                        currGymUser.setComment(comment);
                        currGymUser.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                if(e!=null) Log.e("onCommentDone", e.getMessage());
                                else{
                                    reloadCommentList();
                                }
                            }
                        });
                    } else {
                        Log.e("onCommentDone", e.getMessage());
                        Toast.makeText(GymActivity.this, "Erro: " + e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    //TODO: Not refreshing
    private void reloadCommentList(){
        Log.i("reloadCommentList", "reloading...");
        startLoadingDialog();
        gym.getRelationWithUser(new FindCallback<GymUser>() {
            @Override
            public void done(List<GymUser> relations, ParseException e) {
                stopLoadingDialog();
                if (e == null) {
                    Log.i("reloadCommentList", "relations size: "+gymRelations.size());
                    GymActivity.this.gymRelations = relations;
                    commentAdapter.notifyDataSetChanged();
                } else {
                    // something went wrong
                    Toast.makeText(GymActivity.this, getString(R.string.errorLoading), Toast.LENGTH_LONG).show();
                    GymActivity.this.finish();
                }
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap){
        LatLng gymLocation = new LatLng(gym.getGeoLocation().getLatitude(), gym.getGeoLocation().getLongitude());

        googleMap.addMarker(new MarkerOptions()
                .position(gymLocation)
                .title(gym.getLocal()));
        googleMap.setMyLocationEnabled(true);
        moveToCurrentLocation(googleMap, gymLocation);
    }

    private void moveToCurrentLocation(GoogleMap googleMap, LatLng location){
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 15));
        // Zoom in, animating the camera.
        googleMap.animateCamera(CameraUpdateFactory.zoomIn());
        // Zoom out to zoom level 10, animating with a duration of 2 seconds.
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
    }

    private void setListViewHeightBasedOnChildren(ListView listView){
        ListAdapter mAdapter = listView.getAdapter();

        int totalHeight = 0;
        System.out.println("Adapter "+mAdapter);

        for (int i = 0; i < mAdapter.getCount(); i++) {
            View mView = mAdapter.getView(i, null, listView);
            System.out.println("M View "+mView);
            mView.measure(
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),

                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));

            totalHeight += mView.getMeasuredHeight();
            Log.w("HEIGHT" + i, String.valueOf(totalHeight));

        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight
                + (listView.getDividerHeight() * (mAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    private class CommentAdapter extends BaseAdapter{

        private List<GymUser> relations;
        private LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        private DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        public CommentAdapter(List<GymUser> relationList){
            relations = relationList;
        }

        @Override
        public int getCount() {
            return relations.size();
        }

        @Override
        public Object getItem(int position) {
            return relations.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            final ViewHolder holder;
            if(convertView == null){
                holder = new ViewHolder();
                convertView = inflater.inflate(R.layout.item_adapter_comment, null, false);
                holder.findViews(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            GymUser currentRelation = (GymUser) getItem(position);
            try {
                currentRelation.fetchIfNeeded();
                holder.textComment.setText(currentRelation.getComment());
                holder.textUsername.setText(currentRelation.getUser().fetchIfNeeded().getUsername());
                holder.textDate.setText(dateFormat.format(currentRelation.getUpdatedAt()));
            } catch (ParseException e) {
                e.printStackTrace();
            }


            return convertView;
        }

        class ViewHolder{
            TextView textUsername, textDate, textComment;

            protected void findViews(View contentView){
                textUsername = (TextView) contentView.findViewById(R.id.textNameUser);
                textDate = (TextView) contentView.findViewById(R.id.textDate);
                textComment = (TextView) contentView.findViewById(R.id.textComment);
            }
        }
    }
}
