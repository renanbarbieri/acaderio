package hbsoft.com.br.acaderio;

import android.app.Application;

import com.parse.Parse;
import com.parse.ParseObject;

import hbsoft.com.br.acaderio.model.Gym;
import hbsoft.com.br.acaderio.model.GymUser;

/**
 * Created by Renan on 08/10/2015.
 */
public class AcadeRio extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        // Enable Local Datastore.
        Parse.enableLocalDatastore(this);

        ParseObject.registerSubclass(Gym.class);
        ParseObject.registerSubclass(GymUser.class);

        Parse.initialize(this, getString(R.string.parseApplicationId), getString(R.string.parseClientKey));
    }
}
